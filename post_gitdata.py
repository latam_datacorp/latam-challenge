import requests
import json

url = "https://advana-challenge-check-api-cr-k4hdbggvoq-uc.a.run.app/devops"
data = {
    "name": "jorgemustaine",
    "mail": "jorgescalona@riseup.net.com",
    "github_url": "git remote add origin git@gitlab.com:latam_datacorp\
            /latam_challenge.git"
}

headers = {
    "Content-Type": "application/json"
}

response = requests.post(url, data=json.dumps(data), headers=headers)

print(response.status_code)
print(response.json())

