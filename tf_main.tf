provider "google" {
  project = var.project_id
  region = var.region
}

resource "google_cloud_storage_bucket" "my_bucket" {
  name = "my-bucket"
  location = var.region
}

resource "google_bigquery_dataset" "my_dataset" {
  dataset_id = "my-dataset"
}

resource "google_bigquery_table" "my_table" {
  dataset_id = google_bigquery_dataset.my_dataset.dataset_id
  table_id = "my-table"
  schema = var.schema
}

resource "google_dataflow_job" "my_job" {
  name = "my-job"
  location = var.region
  job_type = "STREAMING"
  template_gcs_path = "gs://my-bucket/my-template.jar"

  environment {
    temp_location = google_cloud_storage_bucket.my_bucket.name
  }

  pipeline {
    streaming_pipeline {
      input {
        pubsub_topic {
          topic = "my-topic"
        }
      }

      output {
        bigquery_table {
          table = google_bigquery_table.my_table.full_table_id
          write_disposition = "WRITE_TRUNCATE"
        }
      }
    }
  }
}

resource "google_cloudscheduler_job" "my_scheduler_job" {
  name = "my-scheduler-job"
  schedule = "0 * * * *"
  target {
    http_target {
      uri = google_dataflow_job.my_job.invoke_http_url
    }
  }
}
