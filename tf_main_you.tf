# Set the provider for Google Cloud Platform
provider "google" {
  credentials = file("<path_to_service_account_key_file>")
  project     = "<your_project_id>"
  region      = "<preferred_region>"
}

# Create a Pub/Sub topic
resource "google_pubsub_topic" "my_topic" {
  name = "<topic_name>"
}

# Create a BigQuery dataset
resource "google_bigquery_dataset" "my_dataset" {
  dataset_id = "<dataset_id>"
  project    = "<your_project_id>"
}

# Grant Pub/Sub publisher role to your service account
resource "google_project_iam_member" "pubsub_publisher" {
  project = "<your_project_id>"
  role    = "roles/pubsub.publisher"
  member  = "serviceAccount:<service_account_email>"
}

# Grant BigQuery data editor role to your service account
resource "google_project_iam_member" "bigquery_data_editor" {
  project = "<your_project_id>"
  role    = "roles/bigquery.dataEditor"
  member  = "serviceAccount:<service_account_email>"
}

# Set up Pub/Sub to BigQuery integration
resource "google_pubsub_topic_iam_member" "bq_integration" {
  topic      = google_pubsub_topic.my_topic.name
  role       = "roles/pubsub.publisher"
  member     = "serviceAccount:cloud-logs@system.gserviceaccount.com"
  depends_on = [google_bigquery_dataset.my_dataset]
}

resource "google_bigquery_dataset_iam_member" "bq_integration" {
  dataset_id = google_bigquery_dataset.my_dataset.dataset_id
  role       = "roles/bigquery.dataViewer"
  member     = "serviceAccount:cloud-logs@system.gserviceaccount.com"
  depends_on = [google_pubsub_topic.my_topic]
}

